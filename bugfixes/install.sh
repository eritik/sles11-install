#!/bin/bash

echo "Enabeling Syslog UDP"
ssh $1 "perl -pi -e 's/#udp\(.*\)/udp\(ip\(\"127\.0\.0\.1\"\) port\(514\))/' /etc/syslog-ng/syslog-ng.conf"
ssh $1 "service syslog reload"