#!/bin/sh
#
# Startup script for Tomcat, the Apache Servlet Engine
# 
# This script is based on:
# http://waelchatila.com/2005/12/13/1134504717808.html 
#
# Written for SLES11 SP2
#

### BEGIN INIT INFO
# Provides: tomcat7 
# Required-Start: $network $local_fs 
# Required-Stop: $network
# Should-Start: $time postgresql sendmail mysql
# Should-Stop: $time postgresql sendmail mysql
# Default-Start: 3 5
# Default-Stop: 0 1 2 6
# chkconfig: 12345 95 05
# Description: The Apache Tomcat Servlet Engine 
### END INIT INFO

MIN_HEAP=64m
MAX_HEAP=8192m
MAX_PERMGEN=512m
TOMCAT_USER=tomcat
JAVA_HOME='/usr/java/default'
CATALINA_HOME='/opt/tomcat7/'

DAEMON_HOME=$CATALINA_HOME/bin/jsvc
TMP_DIR=/var/tmp
PIDFILE=/var/run/jsvc.pid
CATALINA_OPTS=
CLASSPATH=\
$JAVA_HOME/lib/tools.jar:\
$CATALINA_HOME/bin/tomcat-juli.jar:\
$CATALINA_HOME/bin/commons-daemon.jar:\
$CATALINA_HOME/bin/bootstrap.jar

# Initialize variables
RETVAL=0
RUNNING=false

start() {
    echo -n "Starting Tomcat: "
    chown -R $TOMCAT_USER:$TOMCAT_USER /opt/tomcat7/webapps
    chown -R $TOMCAT_USER:$TOMCAT_USER /opt/tomcat7/logs
    chown -R $TOMCAT_USER:$TOMCAT_USER /opt/tomcat7/temp
    chown -R $TOMCAT_USER:$TOMCAT_USER /opt/tomcat7/work            
    $DAEMON_HOME \
    -jvm server \
    -Xms$MIN_HEAP \
    -Xmx$MAX_HEAP \
    -XX:+UseParallelGC \
    -XX:MaxPermSize=$MAX_PERMGEN \
    -user $TOMCAT_USER \
    -home $JAVA_HOME \
    -Dcatalina.home=$CATALINA_HOME \
    -Djava.io.tmpdir=$TMP_DIR \
    -Djava.awt.headless=true \
    -Djava.library.path=$CATALINA_HOME/bin/native/.libs \
    -outfile $CATALINA_HOME/logs/catalina.out \
    -errfile '&1' \
    $CATALINA_OPTS \
    -cp $CLASSPATH \
    org.apache.catalina.startup.Bootstrap

    RETVAL=$?
    if [ $RETVAL = 0 ]; then
        echo "Ok"
    else
        echo "Failed"
    fi
    [ $RETVAL = 0 ] && touch /var/lock/subsys/tomcat
    return $RETVAL
}

# Sets the variable PIDFILE to the value
# specified in $PIDFILE or -1 if there is
# no pid file
pid() {
    PID=-1
    if [ -f $PIDFILE ]
    then
      PID=`cat $PIDFILE`
    fi
}

# Sets the variable RUNNING to true or false
# Depending of there is a process with the
# pid specified in $PIDFILE
is_running() {
    pid
    if [ -f /proc/$PID/status ]; then
        RUNNING=true
    else
        RUNNING=false
    fi
}

# Outputs the current process status
# It basically checks if there is a process running
# with the PID specified in the $PIDFILE
status() {
    is_running
    if [ $RUNNING == true ]; then
	echo "Tomcat is running as PID $PID"
    else
        echo "Tomcat is stopped"
    fi
}

stop() {
    echo -n "Stopping tomcat: "
    PID=`cat /var/run/jsvc.pid`
    kill $PID
    RETVAL=$?
    echo
    [ $RETVAL = 0 ] && rm -f /var/lock/subsys/tomcat /var/run/tomcat.pid
}

# See how we were called.
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  status)
	status
	;;
  restart)
        stop
        # Ugly hack
        # We should really make sure tomcat
        # is stopped before leaving stop
        sleep 5
        start
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac

exit $RETVAL
