This Script installs Tomcat7 on a SLES11 SP2 machine.

Before using this script please make sure that:

  - You have installed Oracle JDK 7 under /usr/java/default
  - You have fixed the following bug: http://www.novell.com/support/kb/doc.php?id=7010013
  - You have enabled udp for the syslog source "src" using "udp(ip("127.0.0.1") port(514));"

To install Tomcat on a server run the script the following way:

  ./install.sh root@host

NOTE: You have to have root privileges with the specified user