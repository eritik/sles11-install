#!/bin/bash

ssh $1 "rm -r tomcat-install-tmp"
scp -qr files $1:tomcat-install-tmp
ssh $1 "chmod +x tomcat-install-tmp/setup.sh"
ssh $1 "./tomcat-install-tmp/setup.sh"
