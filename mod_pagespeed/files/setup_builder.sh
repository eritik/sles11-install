#!/bin/bash

GIT_VERSION=v1.7.12.3

echo "Installing Git (SLES Git is too old)"
cd ~/
git clone git://github.com/git/git.git
cd git
git checkout $GIT_VERSION
make
make install
export PATH=~/bin:$PATH
sleep 5

echo "Installing Depot Tools (required for mod_pagespeed)"
mkdir -p ~/bin
cd ~/bin
svn co http://src.chromium.org/svn/trunk/tools/depot_tools
export PATH=$PATH:~/bin/depot_tools
sleep 5

echo "Checking out mod_pagespeed"
mkdir ~/mod_pagespeed
cd ~/mod_pagespeed
gclient config http://modpagespeed.googlecode.com/svn/tags/1.0.22.7/src
gclient sync --force --jobs=1
sleep 5

echo "Building mod_pagespeed including tests"
cd ~/mod_pagespeed/src
make BUILDTYPE=Release mod_pagespeed_test pagespeed_automatic_test
./out/Release/mod_pagespeed_test
./out/Release/pagespeed_automatic_test
make BUILDTYPE=Release linux_package_rpm
