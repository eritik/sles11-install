#!/bin/bash

ssh $1 "rm -r pagespeed-install-tmp"
scp -qr files $1:pagespeed-install-tmp
ssh $1 "chmod +x pagespeed-install-tmp/setup*.sh"
ssh $1 "./pagespeed-install-tmp/setup.sh"

