#!/bin/bash

echo "Downloading RPM"
ssh $1 'wget -q -c --no-cookies --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F" "http://download.oracle.com/otn-pub/java/jdk/7u7-b10/jdk-7u7-linux-x64.rpm" --output-document="jdk-7u7-linux-x64.rpm"'
ssh $1 "rpm -i jdk-7u7-linux-x64.rpm"
